package handler

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"github.com/friendsofgo/errors"

	"gitlab.com/nouse/url-shortener/storage"

	"github.com/go-chi/chi"
)

// Shortener is to handle all url shorten related functions
type Shortener struct {
	Service storage.URLService
	Prefix  string
}

// Handler return all URL need by Request
func (s Shortener) Handler(middlewares ...func(next http.Handler) http.Handler) http.Handler {
	r := chi.NewRouter()
	r.Use(middlewares...)

	r.Post("/encode", s.encode)

	// Return Info of short code
	r.Get("/info/{code}", s.getInfo)

	r.Get("/{code}", s.get)

	// TODO: GET / will return a POST form, and use a individual app to test
	return r
}

type urlParams struct {
	URL string `json:"url"`
}

func (s Shortener) get(w http.ResponseWriter, r *http.Request) {
	code := chi.URLParam(r, "code")
	ctx := context.Background()

	url, err := s.Service.Load(ctx, storage.Code(code))
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal Error"))
		return
	}

	w.Header().Set("Location", url)
	w.WriteHeader(http.StatusMovedPermanently)
}

func (s Shortener) getInfo(w http.ResponseWriter, r *http.Request) {
	code := chi.URLParam(r, "code")
	ctx := context.Background()

	info, err := s.Service.LoadInfo(ctx, storage.Code(code))
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal Error"))
		return
	}

	body, err := json.Marshal(info)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(body)
}

// Encode passed
func (s Shortener) encode(w http.ResponseWriter, r *http.Request) {
	params := urlParams{}
	err := json.NewDecoder(r.Body).Decode(&params)
	ctx := context.Background()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid input."))
		return
	}
	_, err = url.ParseRequestURI(params.URL)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid URL."))
		return
	}
	code, err := s.Service.Save(ctx, params.URL)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	w.Write([]byte(fmt.Sprintf(`{"code": "%s", "url": "%s"}`, code, code.URL(s.Prefix))))
}
