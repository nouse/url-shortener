package handler

import (
	"context"
	"database/sql"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"gitlab.com/nouse/url-shortener/migrations"
	"gitlab.com/nouse/url-shortener/pgcontainer"

	"gitlab.com/nouse/url-shortener/storage/sqlboiler"

	_ "github.com/lib/pq"
	"go.uber.org/zap"
)

var db *sql.DB

func TestMain(m *testing.M) {
	ctx := context.Background()
	postgresC, err := pgcontainer.NewContainer(ctx)
	defer postgresC.Terminate(ctx)

	db, err = postgresC.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	err = db.Ping()
	if err != nil {
		log.Fatal("ping error", err)
	}
	err = migrations.Run(db)
	if err != nil {
		log.Fatal(err)
	}
	os.Exit(m.Run())
}

func setupHandler(t *testing.T) http.Handler {
	t.Helper()
	logger, _ := zap.NewProduction()
	defer logger.Sync()
	svc := sqlboiler.New(db, logger)

	return Shortener{Service: svc}.Handler()
}

func TestGetIndex(t *testing.T) {
	h := setupHandler(t)
	recorder := httptest.NewRecorder()

	req := httptest.NewRequest("GET", "/", nil)
	h.ServeHTTP(recorder, req)

	if recorder.Code != 404 {
		t.Errorf("Expected GET / to return 404, actual: %d", recorder.Code)
	}
}

func TestGetNonexistCode(t *testing.T) {
	h := setupHandler(t)
	recorder := httptest.NewRecorder()

	req := httptest.NewRequest("GET", "/nonexist", nil)
	h.ServeHTTP(recorder, req)

	if recorder.Code != 404 {
		t.Errorf("Expected GET / to return 404, actual: %d", recorder.Code)
	}
}

func TestPost(t *testing.T) {
	h := setupHandler(t)
	recorder := httptest.NewRecorder()

	postBody := strings.NewReader(`{"url":"http://www.test.com/article/20180718"}`)
	req := httptest.NewRequest("POST", "/encode", postBody)
	h.ServeHTTP(recorder, req)

	if recorder.Code != 200 {
		t.Errorf("Expected GET / to return 404, actual: %d", recorder.Code)
	}

	// TODO get code
}
