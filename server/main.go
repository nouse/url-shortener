package main

import (
	"database/sql"
	"log"
	"net/http"

	"gitlab.com/nouse/url-shortener/storage/sqlboiler"

	"go.uber.org/zap"

	"github.com/go-chi/chi/middleware"
	_ "github.com/lib/pq"
	"gitlab.com/nouse/url-shortener/handler"
)

func main() {
	c := zap.NewProductionConfig()
	c.Encoding = "console"
	logger, _ := c.Build()
	defer logger.Sync()

	zap.RedirectStdLog(logger)()

	db, err := sql.Open("postgres", "host=localhost dbname=urls")
	if err != nil {
		log.Fatal(err.Error())
	}
	svc := sqlboiler.New(db, logger)

	logger.Info("Booting up server", zap.String("version", "v1.0"))
	shortener := handler.Shortener{Service: svc, Prefix: "http://localhost:8080"}
	logger.Info("Shortener:", zap.String("prefix", shortener.Prefix))
	r := shortener.Handler(middleware.RequestID, middleware.Logger)

	err = http.ListenAndServe(":8080", r)
	if err != http.ErrServerClosed {
		log.Fatal(err)
	}
}
