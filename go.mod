module gitlab.com/nouse/url-shortener

go 1.15

require (
	github.com/friendsofgo/errors v0.9.2
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/kat-co/vala v0.0.0-20170210184112-42e1d8b61f12
	github.com/lib/pq v1.9.0
	github.com/spf13/viper v1.6.3
	github.com/testcontainers/testcontainers-go v0.9.0
	github.com/volatiletech/randomize v0.0.1
	github.com/volatiletech/sqlboiler/v4 v4.4.0
	github.com/volatiletech/strmangle v0.0.1
	go.uber.org/zap v1.16.0
)
