package sqlboiler

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"testing"

	"gitlab.com/nouse/url-shortener/pgcontainer"

	"gitlab.com/nouse/url-shortener/migrations"

	"github.com/friendsofgo/errors"
	_ "github.com/lib/pq"
	"gitlab.com/nouse/url-shortener/base58"
	"gitlab.com/nouse/url-shortener/storage"
	"go.uber.org/zap"
)

var db *sql.DB

func TestMain(m *testing.M) {
	// call flag.Parse() here if TestMain uses flags

	ctx := context.Background()
	postgresC, err := pgcontainer.NewContainer(ctx)
	defer postgresC.Terminate(ctx)

	db, err = postgresC.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	err = db.Ping()
	if err != nil {
		log.Fatal("ping error", err)
	}
	err = migrations.Run(db)
	if err != nil {
		log.Fatal(err)
	}
	os.Exit(m.Run())
}

func setupService(t *testing.T) storage.URLService {
	t.Helper()
	logger, _ := zap.NewProduction()
	defer logger.Sync()
	return New(db, logger)
}

func TestLoadNonExist(t *testing.T) {
	svc := setupService(t)
	ctx := context.Background()
	url, err := svc.Load(ctx, "nonexist")
	if !errors.Is(err, sql.ErrNoRows) {
		t.Errorf("Expected to return ErrorNotFound, got %v", err)
	}
	if url != "" {
		t.Errorf("Expected to return empty url, but got %s", url)
	}
}

func TestLoadInfoNonExist(t *testing.T) {
	svc := setupService(t)
	ctx := context.Background()
	item, err := svc.LoadInfo(ctx, "nonexist")
	if !errors.Is(err, sql.ErrNoRows) {
		t.Errorf("Expected to return ErrorNotFound, got %v", err)
	}
	if item.URL != "" {
		t.Errorf("Expected to return empty url, but got %s", item.URL)
	}
}

func TestSaveAndLoad(t *testing.T) {
	svc := setupService(t)
	ctx := context.Background()
	url := fmt.Sprintf("https://www.test.com/news/article/%s", base58.RandomCode())
	code, err := svc.Save(ctx, url)
	if err != nil {
		t.Error(err.Error())
	}

	loadedURL, err := svc.Load(ctx, code)
	if err != nil {
		t.Error(err.Error())
	}
	if loadedURL != url {
		t.Errorf("Expected url %s, but got %s", url, loadedURL)
	}
}

func verifyDetail(detail storage.Item, count int, visited bool, t *testing.T) {
	t.Helper()
	if detail.Count != count {
		t.Errorf("Expected detail count be %d, but got %d", count, detail.Count)
	}
	if detail.Visited != visited {
		t.Errorf("Expected detail visited be %v, but got %v", visited, detail.Visited)
	}
}

// LoadInfo will not update visited, but Load will
func TestSaveAndLoadInfo(t *testing.T) {
	svc := setupService(t)
	url := fmt.Sprintf("https://www.test.com/news/article/%s", base58.RandomCode())
	ctx := context.Background()
	code, err := svc.Save(ctx, url)
	if err != nil {
		t.Error(err.Error())
	}

	detail, err := svc.LoadInfo(ctx, code)
	if err != nil {
		t.Error(err.Error())
	}
	verifyDetail(detail, 0, false, t)

	loadedURL, err := svc.Load(ctx, code)
	if err != nil {
		t.Error(err.Error())
	}
	if loadedURL != url {
		t.Errorf("Expected url %s, but got %s", url, loadedURL)
	}

	detail, err = svc.LoadInfo(ctx, code)
	if err != nil {
		t.Error(err.Error())
	}
	verifyDetail(detail, 1, true, t)

	// Load 3 more times
	for i := 0; i < 3; i++ {
		_, err = svc.Load(ctx, code)
		if err != nil {
			t.Fatal(err)
		}
	}
	detail, err = svc.LoadInfo(ctx, code)
	if err != nil {
		t.Error(err.Error())
	}
	verifyDetail(detail, 4, true, t)
}
