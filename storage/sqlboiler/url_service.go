package sqlboiler

import (
	"context"
	"database/sql"
	"net/url"

	_ "github.com/lib/pq"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/nouse/url-shortener/base58"
	"gitlab.com/nouse/url-shortener/models"
	"gitlab.com/nouse/url-shortener/storage"
	"go.uber.org/zap"
)

type Shortener struct {
	db     *sql.DB
	logger *zap.Logger
}

// New will return new service
func New(db *sql.DB, logger *zap.Logger) Shortener {
	return Shortener{
		db:     db,
		logger: logger,
	}
}

func (s Shortener) Save(ctx context.Context, str string) (storage.Code, error) {
	_, err := url.ParseRequestURI(str)
	if err != nil {
		return "", err
	}
	code := base58.RandomCode()
	m := models.Shortener{
		Code: code,
		URL:  str,
	}
	err = m.Insert(ctx, s.db, boil.Infer())
	if err != nil {
		return "", err
	}
	return storage.Code(code), err
}

func (s Shortener) Load(ctx context.Context, code storage.Code) (string, error) {
	r, err := models.Shorteners(qm.Where("code = ?", code)).One(ctx, s.db)
	if err != nil {
		return "", err
	}
	r.Visited = true
	r.Count += 1
	_, err = r.Update(ctx, s.db, boil.Blacklist("url"))
	return r.URL, err
}

func (s Shortener) LoadInfo(ctx context.Context, code storage.Code) (storage.Item, error) {
	r, err := models.Shorteners(qm.Where("code = ?", code)).One(ctx, s.db)
	if err != nil {
		return storage.Item{}, err
	}
	return storage.Item{
		URL:     r.URL,
		Count:   r.Count,
		Visited: r.Visited,
	}, nil
}
