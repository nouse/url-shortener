// Package storage provide abstract types to handle storage
package storage

import (
	"context"
	"fmt"
)

// Item is type of origin URL.
type Item struct {
	URL     string `json:"url"`
	Visited bool   `json:"visited"`
	Count   int    `json:"count"`
}

// Code is short code of URL, may include some special format.
type Code string

// URL return a url based on code
func (c Code) URL(prefix string) string {
	return fmt.Sprintf("%s/%s", prefix, c)
}

// URLService is the Type to interact with Database
type URLService interface {

	// Save an URL, return short code and error
	Save(context.Context, string) (Code, error)

	// Use code to return URL
	Load(context.Context, Code) (string, error)

	// Return detailed info of code
	LoadInfo(context.Context, Code) (Item, error)
}
