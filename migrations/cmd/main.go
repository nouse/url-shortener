package main

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
	"gitlab.com/nouse/url-shortener/migrations"
)

func main() {
	db, err := sql.Open("postgres", `dbname=shortener sslmode=disable`)
	if err != nil {
		log.Fatal(err)
	}
	err = migrations.Run(db)
	if err != nil {
		log.Fatal(err)
	}
}
