package migrations

import (
	"context"
	"database/sql"
	"fmt"
)

var migrations = []string{
	// language=PostgreSQL
	`CREATE TABLE shortener (
	code varchar(32) PRIMARY KEY,
	url text not null,
	visited bool not null default false,
	count int default 0 not null 
);`,
}

func Run(db *sql.DB) error {
	ctx := context.Background()
	for _, m := range migrations {
		_, err := db.ExecContext(ctx, m)
		if err != nil {
			return fmt.Errorf("fail to run migration, err %w", err)
		}
	}
	return nil
}
