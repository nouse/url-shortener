package base58

import "testing"

func TestRandomCode(t *testing.T) {
	code := RandomCode()
	if len(code) != 6 {
		t.Errorf("Expected code return length 6, but is %s", code)
	}
}
