package base58

import (
	"math/rand"
	"time"
)

const (
	base    = 58
	divider = 3_741_233_577
	pad     = 656_356_768
)

var (
	randSource *rand.Rand
	source     = []byte("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz")
)

func init() {
	randSource = rand.New(rand.NewSource(time.Now().UnixNano()))
}

func RandomCode() string {
	b := make([]byte, 6)
	n := randSource.Int63n(divider) + pad

	for i := 5; i >= 0; i-- {
		r := n % base
		b[i] = source[r]
		n = n / base
	}

	return string(b)
}
