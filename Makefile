.PHONY: web test gomod migrate clean

SHELL=/bin/bash
migrate:
	go run migrations/cmd/main.go

test:
	go test ./...

web:
	go run server/main.go

cov:
	go test -coverprofile=cov ./...
