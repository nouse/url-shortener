* Migrate

#+BEGIN_SRC shell
make migrate
#+END_SRC

* Generate models code

Require Go 1.16
#+BEGIN_SRC shell
go install github.com/volatiletech/sqlboiler/v4/drivers/sqlboiler-psql@v4.4.0
go install github.com/volatiletech/sqlboiler/v4@v4.4.0
sqlboiler psql
#+END_SRC

* Test

#+BEGIN_SRC shell
make test
#+END_SRC

** Coverage

#+BEGIN_SRC shell
make cov
go tool cover -html=cov
#+END_SRC

* Start web

#+BEGIN_SRC shell
make web
#+END_SRC

* Future reading

[[https://searchengineland.com/analysis-which-url-shortening-service-should-you-use-17204]]
