package pgcontainer

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"math/rand"

	"github.com/testcontainers/testcontainers-go/wait"

	"github.com/testcontainers/testcontainers-go"
	"github.com/volatiletech/randomize"
)

type Container struct {
	password string
	testcontainers.Container
}

const (
	natPort = "5432/tcp"
	image   = "postgres:13.2"
)

func NewContainer(ctx context.Context) (*Container, error) {
	password := randomize.Str(rand.Int63, 64)

	req := testcontainers.ContainerRequest{
		ExposedPorts: []string{natPort},
		Image:        image,
		Env: map[string]string{
			"POSTGRES_PASSWORD": password,
		},
		WaitingFor: wait.ForListeningPort(natPort),
	}
	postgresC, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		return nil, err
	}
	return &Container{
		password:  password,
		Container: postgresC,
	}, nil
}

func (c Container) Connect(ctx context.Context) (*sql.DB, error) {
	ip, err := c.Host(ctx)
	if err != nil {
		log.Fatal(err)
	}
	port, err := c.MappedPort(ctx, "5432")
	if err != nil {
		log.Fatal(err)
	}
	return sql.Open("postgres", fmt.Sprintf(`host=%s port=%d password=%s user=postgres sslmode=disable`, ip, port.Int(), c.password))
}
